/**
 * INPUT
 * 
 * nhập vào lần lược chiều dài và rộng của hình chủ nhật
 * chiều dài là 4
 * chiều rộng là 6
 * 
 * TODO
 * 
 * Công thức chu vi là; ( dài + rộng)*2
 *  Công thức tính diện tích là ; dài*rộng
 * 
 * OUTPUT
 * 
 * chu vi là 20
 * diện tích là 24
 * 
 */
 var a = 4;
 var b = 6;
 c = (a+b)*2;
 d = a*b;
 console.log("chu vi hình chữ nhật", c);
 console.log("diện tích hình chữ nhật", d);